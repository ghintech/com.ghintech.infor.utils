package com.ghintech.infor.utils.callout;

import java.math.BigDecimal;

import org.compiere.model.MAcctSchema;
import org.compiere.model.MClientInfo;
import org.compiere.model.MConversionRate;
import org.compiere.model.MInvoice;
import org.compiere.util.CLogger;

import com.ghintech.infor.utils.base.CustomCallout;

public class GetCurrecyRateFromInvoice extends CustomCallout{

	protected CLogger			log = CLogger.getCLogger (getClass());
	@Override
	protected String start() {
		// TODO Auto-generated method stub
		
		if (getValue() == null)
			return "";
		if(getTab().getValue("C_Invoice_ID") == null)
			return "";
		if((Integer)getTab().getValue("C_Invoice_ID")==0) 
			return"";
		//MPayment payment=new MPayment(getCtx(), (Integer)getTab().getValue("C_Payment_ID"),null);
		
			MInvoice invoice = new MInvoice(getCtx(), (Integer)getTab().getValue("C_Invoice_ID"), null);
			int CurFrom_ID=(Integer)getTab().getValue("C_Currency_ID");
			int CurTo_ID=MAcctSchema.get(getCtx(), MClientInfo.get(invoice.getAD_Client_ID()).getC_AcctSchema1_ID()).getC_Currency_ID();
			BigDecimal invoicerate=invoice.getCurrencyRate();
			//log.severe(CurFrom_ID+""+CurTo_ID+""+
			//		invoice.getDateAcct()+""+ invoice.getC_ConversionType_ID()+ ""+invoice.getAD_Client_ID()+ ""+
			//		invoice.getAD_Org_ID());
			if(invoicerate.compareTo(BigDecimal.ZERO)==0) {
				invoicerate= MConversionRate.getRate(CurFrom_ID,CurTo_ID, 
					invoice.getDateAcct(), invoice.getC_ConversionType_ID(), invoice.getAD_Client_ID(), 
					invoice.getAD_Org_ID());
			}
			getTab().setValue("CurrencyRate", invoicerate);
			getTab().setValue("C_ConversionType_ID",invoice.getC_ConversionType_ID());
			BigDecimal	PayAmt= (BigDecimal)getTab().getValue("PayAmt");
			getTab().setValue("ConvertedAmt",PayAmt.multiply(invoicerate));
			
		
		return null;
	}

	

}
