package com.ghintech.infor.utils.callout;

import java.math.BigDecimal;

import org.compiere.model.MAcctSchema;
import org.compiere.model.MAcctSchemaDefault;
import org.compiere.model.MClient;
import org.compiere.model.MClientInfo;
import org.compiere.model.MConversionRate;
import org.compiere.model.MConversionRateUtil;
import org.compiere.model.MCurrency;
import org.compiere.model.MInvoice;
import org.compiere.model.MPayment;
import org.compiere.model.MRMA;
import org.compiere.util.DB;

import com.ghintech.infor.utils.base.CustomCallout;

public class GetCurrecyRateFromRMA extends CustomCallout{

	@Override
	protected String start() {
		// TODO Auto-generated method stub
		
		if (getValue() == null)
			return "";
		
		if(getTab().getValue("C_Invoice_ID") == null)
			return "";
		
		if((Integer)getTab().getValue("C_Invoice_ID")==0) 
			return"";
		
		MInvoice invoice = new MInvoice(getCtx(), (Integer)getTab().getValue("C_Invoice_ID"), null);
		
		if(invoice.getM_RMA_ID()==0)
			return null;
		String sql ="select max(c_invoice_id) from c_invoiceline where m_inoutline_id in (select m_inoutline_id from m_inoutline where m_inout_id="+invoice.getM_RMA().getInOut_ID()+")";
		int invoiceFrom_ID=DB.getSQLValue(null, sql);
		
		MInvoice invoiceFrom = new MInvoice(getCtx(), invoiceFrom_ID, null);
	
		
		
		int CurFrom_ID=invoiceFrom.getC_Currency_ID();
		
		int CurTo_ID=MAcctSchema.get(getCtx(),MClientInfo.get(invoice.getAD_Client_ID()).getC_AcctSchema1_ID()).getC_Currency_ID();
		BigDecimal currencyRate=invoiceFrom.getCurrencyRate();
		if(currencyRate.compareTo(BigDecimal.ZERO)==0) {
			currencyRate=MConversionRate.getRate(CurFrom_ID,CurTo_ID, 
					invoiceFrom.getDateInvoiced(), invoiceFrom.getC_ConversionType_ID(), invoice.getAD_Client_ID(), 
					invoice.getAD_Org_ID());
		}
		setValue("CurrencyRate",currencyRate);
		setValue("C_ConversionType_ID",invoiceFrom.getC_ConversionType_ID());
		
		return null;
	}

	

}
